# FSW7-challenge06-WahyuEko
Description..
This task is still not finished, it is still in the process.


## How to run
Untuk menjalankan development server, tinggal jalanin salah satu script di package.json.

```
-- npm start
```


## Endpoints

Terdapat beberapa endpoint:

##### POST `http://localhost:3000/api/users/register-super-admin`

##### POST `http://localhost:3000/api/users/register-admin`

##### POST `http://localhost:3000/api/users/register-member`

##### POST `http://localhost:3000/api/users/login-super-admin` 

##### POST `http://localhost:3000/api/users/login-admin`

##### POST `http://localhost:3000/api/users/login-member`

##### GET `http://localhost:3000/api/users/getuser`

##### GET `http://localhost:3000/api/cars`

##### GET `http://localhost:3000/api/cars/:id`

##### DELETE `http://localhost:3000/api/cars/:id`

##### POST `http://localhost:3000/api/cars`

##### PUT `http://localhost:3000/api/cars/:id`


## DB Manage

Di dalam repository ini sudah terdapat beberapa script yang dapat digunakan dalam memanage database, yaitu:

- `npm run db:seed-users` untuk melakukan seeding
- `npm run db:seed-cars` untuk melakukan seeding


## Documentation

Documentation https://documenter.getpostman.com/view/20526468/UyxhoSva
