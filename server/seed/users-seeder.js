const bcrypt = require("bcryptjs");

const User = require("../model/user");
const mongoose = require('mongoose');

// mongodb connection
mongoose.connect('mongodb://localhost:27017/dbMobil')

let user = [
    new User({
        username: "superadmin",
        email: "superadmin@gmail.com",
        name: "super admin",
        password: bcrypt.hashSync("superadmin", 12),
        role: "superadmin",
    })
];

// save database
let done = 0;
for (let i = 0; i < user.length; i++) {
    user[i].save(function (err, result) {
        done++;
        if (done === user.length) {
            exit();
        }
    });
}

function exit() {
    mongoose.disconnect();
}